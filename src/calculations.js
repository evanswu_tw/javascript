export const max = (array) => {
    // how about default array to empty list, in this case we dont even need to worry about
    // using == or ===
    // and as advised by the javascript community we should
    // always use the === instead of == since === check both type and the actual value
    // in addition, most of the modern eslint will check for the usage of === too.

    // usually == is should not be used in js. But in this situation
    // the == is much more useful. Because array == undefined equals
    // array === undefined || array === null;
    // this is how jquery works in their source code.

    // good start default the array 
    // but i think this line is redundant 
    array = array || [];
    if (array.length === 0) {
        return null;
    }
    return array.reduce((current, next) => {
        return current > next ? current : next
    });
};


export const average = (array = []) => {

    // This is the trick part. In the es6, we can use default parameter like array = [] in this function;
    // but when array === null, array = [] do not work, this is only works for undefined, which will fails the test.
    // In the js, this is strange, typeof null === "object" works. But typeof undefined === "undefined";
    // so the just should use     array = array || [] here, it can solve the problem.
    // About the ==, you are very right. What I write is quite trick, not a very friendly to others.

    // same as the above comment regarding to default array
    if (array.length === 0) {
        return null;
    }
    return array.reduce((sum, next) => sum + next) / array.length;
};


export const length = (array = []) => {
    // good start default the array 
    // but i think this line is redundant 
    array = array || [];
    // same as above
    if (array.length === 0) {
        return 0;
    }
    return array.length;

};
