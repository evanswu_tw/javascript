import {expect} from 'chai';

import {max, average, length} from './calculations';

describe('calculations', () => {
    describe('max', () => {
        context('when numbers are undefined', () => {
            it('returns null', () => {
                expect(max(undefined)).to.eql(null);
            });
        });
        context('when numbers are null', () => {
            it('return null', () => {
                expect(max(null)).to.eql(null);
            });
        });
        context('when pass in an array', () => {
            it('return max number', () => {
                expect(max([10, 40, 3, 5])).to.eql(40);
            });
        });
    });

    describe('average', () => {
        context('when numbers are undefined', () => {
            it('returns null', () => {
                expect(average(undefined)).to.eql(null);
            });
        });
        context('when numbers are null', () => {
            it('return null', () => {
                expect(average(null)).to.eql(null);
            });
        });
        context('when pass in an array', () => {
            it('return average number', () => {
                expect(average([1, 2, 3])).to.eql(2);
            });
        });

    })


    describe('length', () => {
        context('when numbers are undefined', () => {
            it('returns null', () => {
                expect(length(undefined)).to.eql(0);
            });
        });
        context('when numbers are null', () => {
            it('return null', () => {
                expect(length(null)).to.eql(0);
            });
        });
        context('when pass in an array', () => {
            it('return length number', () => {
                expect(length([1, 2, 3])).to.eql(3);
            });
        });

    })

});
