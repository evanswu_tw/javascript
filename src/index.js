import './index.html';
import parse from './parse';
import {average, max, length} from './calculations';

const getInput = () => parse(document.getElementById('input-box').value);

const setResult = (value) => {
    document.getElementById('result-value').textContent = value;
};

document.getElementById('max').addEventListener('click', () => setResult(max(getInput())));

// the four lines below perform the same purpose so you can just pick one way or the other
document.getElementById('length').addEventListener('click', () => setResult(length(getInput())));
document.getElementById('average').addEventListener('click', () => setResult(average(getInput())));

document.getElementById('length').onclick = () => setResult(length(getInput()));
document.getElementById('average').onclick = () => setResult(average(getInput()));
